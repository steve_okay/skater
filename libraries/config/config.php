<?php

namespace incentro\skaterAPI\Config;

class Config{

    const DB_HOST                     = '127.0.0.1';
    const DATABASE                    = 'skaters';
    const DB_USERNAME                 = 'root'; 
    const DB_PASSWORD                 = ''; 

    const NOT_FOUND                   = 404; // error code
    const ERROR_ON_REQUEST            = 500; // internal server error while performing request
    const CREATED_SUCCESSFULLY        = 201; // created successfully
    const SUCCESS                     = 200; // success code
    const BAD_REQUEST                 = 400; // Bad Request
    const NOT_AUTHORIZED              = 403; // not authorized code
    const DUPLICATE                   = 409; // duplicate entry
    const PAYMENT_REQUIRED            = 402; // payment required
    const NOT_ALLOWED                 = 405; // not allowed

}