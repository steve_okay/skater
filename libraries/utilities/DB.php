<?php

namespace incentro\skaterAPI\Libraries\Utilities;

use incentro\skaterAPI\Config\Config;

class DB{
    
    public static function getDBConnection(){
        
        $dbh = false;

        try{

            $host     = Config::DB_HOST;
            $database = Config::DATABASE;
            $username = Config::DB_USERNAME;
            $password = Config::DB_PASSWORD;

            $dbh = new \PDO("mysql:host=$host;dbname=$database", $username, $password);

        }catch(\PDOException $ex){
            $dbh = false;
            $message = 'PDOException '.$ex->getMessage();
            
        }catch(Exception $ex){
            $dbh = false;
            $message = 'Exception '.$ex->getMessage();
            
        }
        return $dbh;
    }
}
