<?php

namespace incentro\skaterAPI\Libraries\Utilities;

class Utilities{
    /**
     * function format response
     */
    public static function apiResponse($response){
        
        http_response_code($response->code);
        header("Content-type:application/json");
        echo json_encode($response,JSON_NUMERIC_CHECK);

        //flush the buffer after we're done outputting
        ob_flush();
        flush();
    }

    public static function response($code, $message, $data, $pagination){
        $response = array();

        //code must always be there
        $response['code'] = $code;

        if(isset($message)){
            $response['message'] = $message;
        } 

        if(isset($data)){
            $response['data'] = $data;
        }
        
        return (object) $response;
    }
}