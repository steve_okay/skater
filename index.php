<?php

require_once __DIR__ . '/libraries/vendor/autoload.php';

use \Klein\Klein as Router;

use incentro\skaterAPI\Libraries\Utilities\Utilities;
use incentro\skaterAPI\Libraries\Utilities\DB;
use incentro\skaterAPI\Config\Config;
use incentro\skaterAPI\Users;

class skaterAPI{

    //variable to handle routing
    public $route;
    public $app;

    function __construct(){

        //variable to handle the request that comes to the application
        $post = $_REQUEST;

        //variable to hold the payload
        $payload = (object) $_REQUEST;

        //define a router class
        $this->route = new Router(); 

        //since we will be using a database
        // we initialize connection details in the constructor
        $dbh = DB::getDBConnection();
        
        //object to hold connection details
        $this->app = (object) array(
            'dbh'       => $dbh,
            'payload'   => $payload,
            'message'   => NULL,
            'code'      => Config::NOT_FOUND
        );

        //assuming the ednpoints are validated and only the authorised endpoints have been called then,
        $this->routeToAPI();

        // handle 404s
        $this->route->respond('404', function ($request, $httpResponse) {
            $message = 'Oops, it looks the page doesn\'t exist..';
            
            $response = (object) array(
                        'code' => Config::NOT_FOUND,
                        'message' => $message
                    );

            //format response and return to user
            Utilities::apiResponse($response);

            //http code
            $httpResponse->code($response->code);
        });

        $this->route->dispatch();
    }

    /**
     * method to handle all routing functionalities
     * @param null
     * @return null
     */
    public function routeToAPI(){

        //endpoint to add user
        $this->route->respond('POST', '/api/users', function($request, $httpResponse){
            $response = Users::createUser($this->app);

            Utilities::apiResponse($response);
            $httpResponse->code($response->code);
        });

        //endpoint to add a trick
        $this->route->respond('POST','/api/tricks',function($request, $httpResponse){
            $response = Users::createTricks($this->app);

            Utilities::apiResponse($response);
            $httpResponse->code($response->code);
        });

        //endpoint to list tricks
        $this->route->respond('GET','/api/tricks',function($request, $httpResponse){
            $response = Users::fetchTricks($this->app);

            Utilities::apiResponse($response);
            $httpResponse->code($response->code);
        });

        //endpoint to favourite/unfavourite a trick
        //endpoint to list tricks
        $this->route->respond('POST','/api/tricks/[i:id]',function($request, $httpResponse){
            $response = Users::favoriteTrick($this->app);

            Utilities::apiResponse($response);
            $httpResponse->code($response->code);
        });

    }
}
