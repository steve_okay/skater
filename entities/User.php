<?php

namespace incentro\skaterAPI;

use incentro\skaterAPI\Libraries\Utilities\Utilities;
use incentro\skaterAPI\Config\Config;

class Users{
    
    /**
     * function to create user and add to db
     * $param array()
     */
    public static function createUser($app){
        
        //expected payload format {username:xxx,age:xxx, email:xxx}
        $data = array();

        try{
            
            $query = 'INSERT INTO users (username,age,email,dateCreated)
                    VALUES(:username,:age,:email,:datecreated)';

            $app->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $app->dbh->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

            $stmt = $app->dbh->prepare($query);

            $stmt->bindParam(':username',$app->payload->username,\PDO::PARAM_STR);
            $stmt->bindParam(':age',$app->payload->age,\PDO::PARAM_STR);
            $stmt->bindParam(':email',$app->payload->email,\PDO::PARAM_STR);
            $stmt->bindParam(':datecreated',date("Y/m/d"),\PDO::PARAM_STR);


            if($stmt->execute()){

                $userID = $app->dbh->lastInsertId();
                
                $app->message = 'User created successfully';
                $app->code = Config::CREATED_SUCCESSFULLY;
                $data = array(
                    'id' => (int) $userID
                );
            }

        }catch(\PDOException $ex){

            $app->message = 'An error occured while processing this request '.$ex->getMessage();; 
            $app->code = Config::ERROR_ON_REQUEST;
            
        }catch(Exception $ex){

            $app->message = 'Error creating user General Exception: '.$ex->getMessage();
            $app->code = Config::ERROR_ON_REQUEST;

        }

        //format the response
        $response = Utilities::response($app->code,$app->message,$data);

        return $response;

    }

    /**
     * function to add a trick
     * @param object
     * @return object
     */
    public static function createTricks($app){

        $data = array();

        try{
            
            $query = 'INSERT INTO tricks (userID,trickName,trickAttachment,dateCreated,favorite)
                    VALUES(:userID,:trickName,:attachment,:datecreated,:favorite)';

            $app->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $app->dbh->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

            $stmt = $app->dbh->prepare($query);

            $stmt->bindParam(':userID',$app->payload->userID,\PDO::PARAM_STR);
            $stmt->bindParam(':trickName',$app->payload->trickName,\PDO::PARAM_STR);
            $stmt->bindParam(':attachment',$app->payload->attachment,\PDO::PARAM_STR);
            $stmt->bindParam(':datecreated',date("Y/m/d"),\PDO::PARAM_STR);
            $stmt->bindParam(':favorite',false,\PDO::PARAM_STR);


            if($stmt->execute()){
                
                $app->message = 'Trick successfully added to the selected user';
                $app->code = Config::CREATED_SUCCESSFULLY;
            }

        }catch(\PDOException $ex){

            $app->message = 'An error occured while processing this request '.$ex->getMessage();; 
            $app->code = Config::ERROR_ON_REQUEST;
            
        }catch(Exception $ex){

            $app->message = 'Error creating user General Exception: '.$ex->getMessage();
            $app->code = Config::ERROR_ON_REQUEST;

        }

        //format the response
        $response = Utilities::response($app->code,$app->message,$data);

        return $response;
    }

    /**
     * function to fetch tricks with specified limits
     */
    public  static function fetchTricks($app){

        $data = array();

        try{
            
            $query = 'SELECT a.trickName,b.username FROM tricks as a INNER JOIN users as b on a.userID = b.userID WHERE b.username = :username ORDER BY a.trickID DESC LIMIT :limit';

            $app->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $app->dbh->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

            $stmt = $app->dbh->prepare($query);

            $stmt->bindParam(':username',$app->payload->username,\PDO::PARAM_STR);
            $stmt->bindParam(':limit',$app->payload->limit,\PDO::PARAM_STR);


            if($stmt->execute()){
                
                //data returned from the select query
                $data = $stmt->fetch(\PDO::FETCH_ASSOC);
                $app->code = Config::SUCCESS;
                $app->message = "Successfully fetched data for specified user";

                // incase we find no results
                if($data == false){

                    $data = array();
                    $app->code = Config::SUCCESS;
                    $app->message ="No trick found for specified user";

                }

            }else{

                $app->message = "Error while fetching tricks";
                $app->code = Config::BAD_REQUEST;
            }

        }catch(\PDOException $ex){

            $app->message = 'An error occured while processing this request '.$ex->getMessage();; 
            $app->code = Config::ERROR_ON_REQUEST;
            
        }catch(Exception $ex){

            $app->message = 'Error fetching tricks '.$ex->getMessage();
            $app->code = Config::ERROR_ON_REQUEST;

        }

        //format the response
        $response = Utilities::response($app->code,$app->message,$data);

        return $response;
    }


    public static function favoriteTrick($app){

        $data = array();

        try{
        
            $query = "UPDATE tricks SET favorite = :favorite WHERE trickID = :trickID LIMIT 1";

            $stmt = $app->dbh->prepare($query);

            $stmt->bindParam(':favorite',$app->payload->favorite,\PDO::PARAM_INT);
            $stmt->bindParam(':trcikcID',$app->payload->trcikID,\PDO::PARAM_INT);

            if($stmt->execute()){

                $app->message = 'trick updated successfully';
                $app->code = Config::SUCCESS;

            }else{

                $app->message = 'Error Updating trick ';
                $app->code = Config::BAD_REQUEST;
            }

        }catch(Exception $e){

            $app->message = 'Exception Occured when trying to update favorite ';
            $app->code = Config::ERROR_ON_REQUEST;

        }

        //format the response
        $response = Utilities::response($app->code,$app->message,$data);

        return $response;

    }


}