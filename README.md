NB : PROJECT IS A VANILLA PHP PROJECT USING COMPOSER FOR DEPENDENCY MANAGEMENT

HOW TO RUN
-------------
1. cd into the libraries folder
2. Run composer install or composer update
3. On phpmyadmin or any database management system, create database call skaters using below command

    create database `skaters`

4. Create the below tables on the database by running the followibg commands

        create table users(
            id int not null AUTO_INCREMENT PRIMARY KEY,
            username varchar(60),
            age int(10),
            email varchar(120),
            dateCreated DATE
        )

        create table tricks(
            trickID int not null AUTO_INCREMENT PRIMARY KEY,
            userID int not null,
            trickName varchar(60),
            trickAttachment varchar(100),
            dateCreated DATE,
            favorite int(1)
        )

5. Upload the source code into a webserver and configure the server to be able to listen to request and direct to the application on port 8080
6. you can try send a request to the endpoints
7. endpoints include

    POST : /api/users
    POST : /api/tricks
    GET  : /api/tricks
    POST : /api/tricks/id